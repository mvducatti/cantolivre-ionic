import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PersonalDetailPage } from '../pages/signup/personal-detail/personal-detail';
import { ReligiousDetailPage } from '../pages/signup/religious-detail/religious-detail';
import { WorkDetailPage } from '../pages/signup/work-detail/work-detail';
import { StudentDetailPage } from '../pages/signup/student-detail/student-detail';
import { ParentDetailPage } from '../pages/signup/parent-detail/parent-detail';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PersonalDetailPage,
    ReligiousDetailPage,
    WorkDetailPage,
    StudentDetailPage,
    ParentDetailPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    PersonalDetailPage,
    ReligiousDetailPage,
    WorkDetailPage,
    StudentDetailPage,
    ParentDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
